// Import react
import React from 'react';

// React Router
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';

// Import components
import ScrollToTop from './ScrollToTop';
import Header from './components/Header.js';
import Footer from './components/Footer.js';
import Home from './components/pages/Home.js';
import About from './components/pages/About.js';
import Team from './components/pages/Team.js';
import ProductsOverview from './components/pages/ProductsOverview.js';
import Product from './components/pages/Product.js';
import Contact from './components/pages/Contact.js';

// Import CSS styles
import './App.css';

// Render app and contains the router switch which handles route switching by URL path
function App() {
  return (
    <Router>
      <ScrollToTop />
      <Header />
      <Switch>
        <Route path="/about">
          <About />
        </Route>
        <Route path="/team">
          <Team />
        </Route>
        <Route path="/contact">
          <Contact />
        </Route>
        <Route path="/products/:id">
            <Product />
        </Route>
        <Route path="/products">
          <ProductsOverview />
        </Route>
        <Route path="/">
          <Home />
        </Route>
      </Switch>
      <Footer />
    </Router>
  );
}

// Export App component
export default App;
// Import react
import React from 'react';

// Import React Router links
import { Link } from 'react-router-dom';

// Import images
import apron_420w from '../../images/products/apron-420w.webp';
import apron_550w from '../../images/products/apron-550w.webp';
import gloves_328w from '../../images/products/Gloves-328w.webp';
import bags_420w from '../../images/products/Bags-420w.webp';
import bags_486w from '../../images/products/Bags-486w.webp';
import straws_420w from '../../images/products/Straws-420w.webp';
import straws_593w from '../../images/products/Straws-593w.webp';

// Render Biodegrable Aprons product link
function Aprons() {
    return (
        <section className="product-link">
            <Link to="/products/0">
                <div className="product-image-container">
                <img 
                    style={{maxWidth: "550px"}}
                    className="product-link-image" 
                    alt="A woman wearing a translucent Biodegradable Apron"
                    src={apron_420w}
                    srcSet={`${apron_420w} 420w, ${apron_550w} 550w`}
                />
                </div>
                <h2>Biodegradable Aprons</h2>
            </Link>
        </section>
    );
}

// Render Biodegrable Gloves product link
function Gloves() {
    return (
        <section className="product-link">
            <Link to="/products/1">
                <div className="product-image-container">
                    <img 
                        className="product-link-image"
                        alt="White Biodegradable Glove"
                        src={gloves_328w}
                    />
                </div>
                <h2>Biodegradable Gloves</h2>
            </Link>
        </section>
    );
}

// Render Biodegrable Bags product link
function Bags() {
    return (
        <section className="product-link">
            <Link to="/products/2">
                <div className="product-image-container">
                    <img 
                        style={{maxWidth: "486px"}}
                        className="product-link-image" 
                        alt="Translucent Biodegradable Bag"
                        src={bags_420w}
                        srcSet={`${bags_420w} 420w, ${bags_486w} 486w`}
                    />
                </div>
                <h2>Biodegradable Bags</h2>
            </Link>
        </section>
    );
}

// Render Biodegrable Straws product link
function Straws() {
    return (
        <section className="product-link">
            <Link to="/products/3">
                <div className="product-image-container">
                    <img 
                        style={{maxWidth: "593px"}}
                        className="product-link-image" 
                        alt="White Biodegradable Straws"
                        src={straws_420w}
                        srcSet={`${straws_420w} 420w, ${straws_593w} 593w`}
                    />
                </div>
                <h2>Biodegradable Straws</h2>
            </Link>
        </section>
    );
}

// Render products list
function ProductsList() {
    return (
        <section className="products-list">
            {/* Bio Aprons */}
            <Aprons />
            {/* Bio Gloves */}
            <Gloves />
            {/* Bio Bags */}
            <Bags />
            {/* Bio Straws */}
            <Straws />
        </section>
    );
}

// Renders the get in touch call to action aside
function CallToAction() {
    return (
        <aside>
            <h2>
                Worldwide Delivery.<br></br>
                <br></br>
                All Products 100% Biodegradable.<br></br>
                <br></br>
                Get In Touch To Discuss Pricing.
            </h2>
            <div className="main-link-container"><Link className="main-link" to="/contact">Contact us</Link></div>
        </aside>
    );
}

// Renders the products page
function ProductsOveriew() {
    return (
        <main className="products-page-container">
            <h1>Products</h1>
            <CallToAction />
            <ProductsList />
        </main>
    );
}

// Export Products component
export default ProductsOveriew;
// Import react
import React from 'react';

// Import react router link
import { Link } from 'react-router-dom';

// Import images
import ColourLogo_420w from '../../images/logo/ColourLogo-420w.webp';
import ColourLogo_751w from '../../images/logo/ColourLogo-751w.webp';
import turtle_420w from '../../images/gallery/turtle-420w.webp';
import turtle_890w from '../../images/gallery/turtle-890w.webp';
import bag_whale_420w from '../../images/gallery/bag-whale-420w.webp';
import bag_whale_1024w from '../../images/gallery/bag-whale-1024w.webp';
import beach_plastic_420w from '../../images/gallery/beach-plastic-420w.webp';
import beach_plastic_685w from '../../images/gallery/beach-plastic-685w.webp';
import digging_plastic_420w from '../../images/gallery/digging-plastic-420w.webp';
import digging_plastic_1024w from '../../images/gallery/digging-plastic-1024w.webp';

// Render home page banner
function HomeBanner() {
  return(
    <section className="home-top">
      <div>
        <img 
          style={{maxWidth: "751px"}}
          className="main-img" 
          alt="The Biodegradable Store logo."
          src={ColourLogo_420w}
          srcSet={`${ColourLogo_420w} 420w, ${ColourLogo_751w} 751w`}
        />      
      </div>
      <h2>Help Us Help You.<br></br>Get In Touch Today.</h2>
      <div className="main-link-container"><Link className="main-link" to="/contact">Contact us</Link></div>
    </section>
  );
}

// Render the Costing The Earth section
function CostingTheEarth() {
  return (
    <section className="costing-the-earth">
      <h2>Costing The Earth</h2>
      <p>Plastic is currently choking the Earth’s oceans and suffocating the soil. Help us help you. Get in touch today.</p>
      <div className="main-link-container"><Link className="main-link" to="/contact">Contact us</Link></div>
    </section>
  );
}

// Render offers aside
function Offers() {
  return (
    <aside>
      <h2>Free Delivery On Orders Over £100</h2>
      <div className="main-link-container"><Link className="main-link" to="/products">View products</Link></div>
    </aside>
  );
}

// Render home page
function Home() {
  return (
    <main className="home-page-container">
      {/* Top banner logo and call to action */}
      <HomeBanner />
      <div>
        <img 
          style={{maxWidth: "1024px"}}
          className="main-img" 
          alt="A plastic bag floating in the ocean with a whale in the background dwarfed by the bag due to the perspective. 
            The bag looks like an iceberg, with its peak breaching the ocean surface."
          src={bag_whale_420w}
          srcSet={`${bag_whale_420w} 420w, ${bag_whale_1024w} 1024w`}
        />        
      </div>
      {/* Title header */}
      <h1>Welcome To The Bio-Store</h1>
      <p>We are committed to providing biodegradable alternatives to the plastics that are damaging our planet.</p>
      <p>We are based in the North-West of England, but deliver worldwide and specialise in those products that are frequently disposed of all too readily.</p>
      {/* Image */}
      <div>
        <img 
          style={{maxWidth: "685px"}}
          className="main-img" 
          alt="A person holding plastic waste covered in dirt in their hand. More plastic waste in the background is laying 
            on the ground in a shallow hole in the grass." 
          src={beach_plastic_420w}
          srcSet={`${beach_plastic_420w} 420w, ${beach_plastic_685w} 685w`}
        />        
      </div>
      <p>So is it biodegradable gloves you need? Maybe aprons? Or are you in catering and require an alternative to those plastic straws that end up everywhere? Check out our products page!</p>
      
      {/* Free delivery offer aside */}
      <Offers />

      <p>We realise that it’s hard to break habits, but we’re here to discuss your options on how to turn your business into an Earth-friendly one. Contact us today!</p>
      {/* Image */}
      <div>
        <img 
          style={{maxWidth: "1024px"}}
          className="main-img" 
          alt="Three crushed used plastic bottles left on the ground in the grass."
          src={digging_plastic_420w}
          srcSet={`${digging_plastic_420w} 420w, ${digging_plastic_1024w} 1024w`}
        />        
      </div>
      <p>We also offer different services such as:</p>
      <ul>
        <li>Environmentally-friedly cleaning</li>
        <li>Consultation on how to make your business eco-friendly</li>
      </ul>
      <p>There's something to suit everyone but if you can't find what you're looking for don't hesitate to <Link to="/contact">contact us</Link> and we'll find the perfect solution for you!</p>
      {/* Image */}
      <div>
        <img 
          style={{maxWidth: "890px"}}
          className="main-img" 
          alt="A compacted ball of dozens of plastic bottles floating in the ocean with one visible on the surface of the water. 
            There is also an iceberg in the background with a small peak of ice visible at the surface and a large mass below the surface."
          src={turtle_420w}
          srcSet={`${turtle_420w} 420w, ${turtle_890w} 890w`}
        />        
      </div>
      {/* Costing The Earth section */}
      <CostingTheEarth />
    </main>
  );
}

// Export Home component
export default Home;
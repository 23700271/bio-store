// Import react
import React from 'react';

// Render contact form
function ContactForm() {
    return (
        <form className="contact-form">
            <label htmlFor="name">Name</label>
            <input type="text" id="name" name="name" placeholder="Your name..."></input>

            <label htmlFor="email">Email</label>
            <input type="email" id="email" name="email" placeholder="Your email..."></input>

            <label htmlFor="message">Message</label>
            <textarea name="message" placeholder="Your message..." rows="7" cols="35"></textarea>

            <input className="main-link" type="submit" value="Submit"></input>
        </form>
    );
}

// Render phone contact details
function PhoneContact() {
    return(
        <section className="contact-phone">
            <i style={{fontSize: "48px"}} className="material-icons">phone</i>
            <section>
                <h2>Phone</h2>
                <p>0151 883 1122</p>
            </section>
        </section>
    );
}

// Render email contact details
function EmailContact() {
    return(
        <section className="contact-email">
            <i style={{fontSize: "48px"}} className="material-icons">email</i>
            <section>
                <h2>Email</h2>
                <p>info@bio-storeexample.co.uk</p>
            </section>
        </section>
    );
}

// Render address list
function AddressList() {
    return(
        <section className="contact-address">
            <section>
                <h2>Manchester</h2>
                <p>
                    Bio-Store
                    <br></br>
                    15-17 St Andrew Street
                    <br></br>
                    Manchester
                    <br></br>
                    M3 5NS
                </p>
            </section>
            <section>
                <h2>Newcastle</h2>
                <p>
                    Bio-Store
                    <br></br>
                    35 Grey Street
                    <br></br>
                    Newcastle
                    <br></br>
                    NE11 6JB
                </p>
            </section>
            <section>
                <h2>Dublin</h2>
                <p>
                    Bio-Store
                    <br></br>
                    O'Connell Street Lower
                    <br></br>
                    North City
                    <br></br>
                    Dublin 1
                    <br></br>
                    Ireland
                </p>
            </section>
        </section>
    );
}

// Render contact us page
function Contact() {
    return (
        <main className="contact-page-container">
            <h1>Contact us</h1>
            <p>We have a wide range of biodegradable solutions. Feel free to drop into any of 
                our branches, drop us a message on our contact form or give us a ring!</p>
            {/* Contact form */}
            <ContactForm />  
            <section className="contact-details">
                {/* Phone number */}
                <PhoneContact />
                {/* Email address */}
                <EmailContact />
            </section> 
            {/* Address list */}
            <AddressList />
        </main>
    );
}

// Export Contact component
export default Contact;
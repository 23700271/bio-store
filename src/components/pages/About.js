// Import react
import React from 'react';

// Import React Router link
import { Link } from 'react-router-dom';

// Import images
import bottles_iceberg_420w from '../../images/gallery/bottles-iceberg-420w.webp';
import bottles_iceberg_1024w from '../../images/gallery/bottles-iceberg-1024w.webp';
import gloves_420w from '../../images/products/Gloves2-420w.webp';
import gloves_763w from '../../images/products/Gloves2-763w.webp';

// Render contact us aside
function AboutAside() {
    return (
        <aside>
            <h2>Help Us Help You. Get In Touch Today.</h2>
            <div className="main-link-container"><Link className="main-link" to="/contact">Contact us</Link></div>
        </aside>
    );
}

// Renders the about page
function About() {
    return (
        <main className="about-page-container">
                <h1>About us</h1>
                <p>Bio-Store are committed to cutting down on plastic waste and helping our environment. We are constantly looking for new ways to combat climate change and our design team are busy creating new products to help you do the same.</p>
                {/* Image 1 */}
                <div>
                    <img 
                        style={{maxWidth: "1024px"}}
                        className="main-img" 
                        alt="A compacted ball of dozens of plastic bottles floating in the ocean with one visible on the 
                            surface of the water. There is also an iceberg in the background with a small peak of ice visible 
                            at the surface and a large mass below the surface." 
                        src={bottles_iceberg_420w}
                        srcSet={`${bottles_iceberg_420w} 420w, ${bottles_iceberg_1024w} 1024w`}
                    />
                </div>
                <p>Whilst everyone is now aware of the damages of climate change and plastic waste, many don’t know the alternatives. We’re here to help! Consult with us on how you and your business can utilize our products to make you truly eco-friendly.</p>
                {/* Image 2 */}
                <div>
                    <img 
                        style={{maxWidth: "763px"}}
                        className="main-img" 
                        alt="A white Biodegradable Glove being pulled onto a hand" 
                        src={gloves_420w}
                        srcSet={`${gloves_420w} 420w, ${gloves_763w} 763w`}
                    />
                </div>
                <section>
                    <h2>How it works</h2>
                    <p>Drop into our store to see our products, 
                        drop us a message or give us a ring. Our team are ready 
                        to help, whether you want to purchase some of our products or tell us 
                        about you and your business to get advice on how to become more 
                        eco-friendly.</p>                  
                </section>
            <AboutAside />
        </main>
    );
}

// Export About component
export default About;
// Import react
import React from 'react';

// Import React Router useParams 
import { useParams } from "react-router-dom";

// Import images
import apron_420w from '../../images/products/apron-420w.webp';
import apron_550w from '../../images/products/apron-550w.webp';
import gloves_420w from '../../images/products/Gloves2-420w.webp';
import gloves_763w from '../../images/products/Gloves2-763w.webp';
import bags_420w from '../../images/products/Bags-420w.webp';
import bags_486w from '../../images/products/Bags-486w.webp';
import straws_420w from '../../images/products/Straws-420w.webp';
import straws_593w from '../../images/products/Straws-593w.webp';

// Returns the selected product data
// Product data identified by id prop from URL paramater id
function getProduct(id) {
    // Products data array
    const productsList = [
        {
            title: "Biodegradable Aprons",
            img:                     
                <img 
                    style={{maxWidth: "550px"}}
                    className="main-img" 
                    alt="A woman wearing a translucent Biodegradable Apron"
                    src={apron_420w}
                    srcSet={`${apron_420w} 420w, ${apron_550w} 550w`}
                />,
            details:         
                <section>
                    <p>NEW Polycare Biodegradable single use aprons</p>
                    <p>As part of our commitment to the environment and sustainability, we have introduced the new Polycare range of biodegradable aprons. Biodegradable aprons ensure speedier breakdown when discarded into landfill, where the majority of disposable polythene aprons are currently sent following use.</p>
                    <section>
                        <h3>Key Features</h3>
                        <p>Polycare biodegradable aprons are adjustable to fit all shapes and builds. Supplied on rolls for easy dispensing. Polycare biodegradable aprons are for use in all departments and medical specialisations including nursing, midwifery, infection control, domestic, paediatric and theatre.</p>
                        <ul>
                            <li>Standard quantity = 5 rolls of 200 aprons in biodegradable sleeves</li>
                            <li>CE Certified</li>
                            <li>Boxed in cardboard outers which can be recycled</li>
                            <li>18 – 36 months to commence break down once sent to landfill</li>
                        </ul>
                        <p>NOTE: White only is available as of May 2018. Limited quantities of other colours, based on colour coding and waste segregation requirements, are planned to follow later in 2018. Please contact us for availability.</p>
                    </section>
                    <section>
                        <h3>Duty of Care</h3>
                        <p>Important steps have been taken to reduce the amount of virgin polythene and significantly increase the amount of recycled material across our complete range of healthcare disposables.</p>
                        <ul>
                            <li>less polythene pollution of seas and seashores improves wildlife environments and ecosystems</li>
                            <li>reduces the need for incineration, currently a major challenge in the UK due to limited number of sites that can recycle the large tonnage of plastic used</li>
                        </ul>
                        <p>China was the largest importer of the UK’s plastic waste, taking millions of tonnes of plastic a year. Since October 2017 China has ended all imports of UK plastic waste leaving the UK with a serious problem to process and recycle plastic. Currently the only option in many areas is landfill.</p>
                        <p>The UK Government is investing in new recycling projects but is a long way from being able to manage the quantity of plastic waste currently produced. 2042 is the current date for achieving the UK targets. GV Health, as a main approved supplier to the NHS is working with all partners to ensure the most sustainable way forward in terms of product and disposal NOW.</p>
                    </section>
                </section>
        },
        {
            title: "Biodegradable Gloves",
            img:                     
                <img 
                    style={{maxWidth: "763px"}}
                    className="main-img" 
                    alt="A white Biodegradable Glove being pulled onto a hand"
                    src={gloves_420w}
                    srcSet={`${gloves_420w} 420w, ${gloves_763w} 763w`}
                />,
            details: 
                <section>
                    <ul>
                        <li>An economical alternative for those with latex allergies</li>
                        <li>Beaded cuff and smooth surface ensures strength and dexterity</li>
                        <li>Comply with EN455-2</li>
                        <li>Powder free material minimises powder contamination and allergic</li>
                        <li>Industry standard 240mm cuff length</li>
                        <li>Quality management in accordance with ISO 9001 and ISO 13485</li>
                        <li>Supplied in dispenser boxes of 100 ambidextrous gloves</li>
                    </ul>
                </section>
        }, 
        {
            title: "Biodegradable Bags",
            img:                     
                <img 
                    style={{maxWidth: "486px"}}
                    className="main-img" 
                    alt="Translucent Biodegradable Bag"
                    src={bags_420w}
                    srcSet={`${bags_420w} 420w, ${bags_486w} 486w`}
                />,
            details: 
                <section>
                    <ul>
                        <li>An economical alternative for plastic bin bags</li>
                        <li>Smooth surface ensures strength and dexterity</li>
                        <li>Comply with EN455-2</li>
                        <li>Minimises contamination</li>
                        <li>Industry standard thickness</li>
                        <li>Quality management in accordance with ISO 9001 and ISO 13485</li>
                        <li>Supplied in boxes of 250</li>
                    </ul>
                </section>
        }, 
        {
            title: "Biodegradable Straws",
            img:                     
                <img 
                    style={{maxWidth: "593px"}}
                    className="main-img" 
                    alt="White Biodegradable Straws"
                    src={straws_420w}
                    srcSet={`${straws_420w} 420w, ${straws_593w} 593w`}
                />,
            details:
                <section>
                    <ul>
                        <li>An economical alternative for plastic straws</li>
                        <li>Smooth surface ensures strength and dexterity</li>
                        <li>Comply with EN455-2</li>
                        <li>Minimises contamination</li>
                        <li>Industry standard</li>
                        <li>Quality management in accordance with ISO 9001 and ISO 13485</li>
                        <li>Supplied in boxes of 250</li>
                    </ul>
                </section>
        }
    ];

    // Return product data from productsList array
    return productsList[id];
}

// Render selected product
function Product() {
    // Get URL paramater
    let { id } = useParams();
    // Get product information
    let product = getProduct(id);

    // Return HTML content
    return (
        <main className="product-page">
            {/* Product title */}
            <h2>{product.title}</h2>
            {/* Product image */}
            <div>{product.img}</div>
            {/* Product details */}
            {product.details}
        </main>
    );
}

// Export Product component
export default Product;
// Import React
import React from 'react';

// Import images for team member profiles
import alex_carter_420w from '../../images/team/alex-carter-420w.webp';
import alex_carter_500w from '../../images/team/alex-carter-500w.webp';
import harry_robertson_420w from '../../images/team/harry-robertson-420w.webp';
import harry_robertson_500w from '../../images/team/harry-robertson-500w.webp';
import jennifer_robertson_420w from '../../images/team/jennifer-robertson-420w.webp';
import jennifer_robertson_500w from '../../images/team/jennifer-robertson-500w.webp';
import jessica_brotherton_333w from '../../images/team/jessica-brotherton-333w.webp';
import josh_manningham_333w from '../../images/team/josh-manningham-333w.webp';
import laura_lago_333w from '../../images/team/laura-lago-333w.webp';


// Renders Jennifer Robertson's profile
function ProfileJenniferRobertson() {
    return (
        <section className="Team-container">
            <img 
                style={{maxWidth: "500px"}}
                className="Team-picture" 
                alt="Jennifer Robertson wearing a dotted green top and grinning." 
                src={jennifer_robertson_420w}
                srcSet={`${jennifer_robertson_420w} 420w, ${jennifer_robertson_500w} 500w`}
            />
            <section className="Team-profile">
                <h2>Jennifer Robertson</h2>
                <p>Founder of Bio-Store</p>
                <p>MSc in Conservation Management</p>
            </section>
        </section>
    );
}

// Renders Harry Robertson's profile
function ProfileHarryRobertson() {
    return (
        <section className="Team-container">
            <img 
                style={{maxWidth: "500px"}}
                className="Team-picture" 
                alt="Harry Robertson wearing black glasses, a white shirt and smiling." 
                src={harry_robertson_420w}
                srcSet={`${harry_robertson_420w} 420w, ${harry_robertson_500w} 500w`}
            />
            <section className="Team-profile">
                <h2>Harry Robertson</h2>
                <p>Production Manager</p>
                <p>BA in Business </p>
            </section>
        </section>
    );
}

// Renders Josh Manningham's profile
function ProfileJoshManningham() {
    return (
        <section className="Team-container">
            <img 
                style={{maxWidth: "333px"}}
                className="Team-picture" 
                alt="Josh Manningham smiling on a beach." 
                src={josh_manningham_333w}
            />
            <section className="Team-profile">
                <h2>Josh Manningham</h2>
                <p>Store Team Manager</p>
                <p>BA in Retail Management</p>
            </section>
        </section>
    );
}

// Renders Alex Carter's profile
function ProfileAlexCarter() {
    return (
        <section className="Team-container">
            <img 
                style={{maxWidth: "500px"}}
                className="Team-picture" 
                alt="Alex Carter smiling with her arms crossed in front of her wearing a black top." 
                src={alex_carter_420w}
                srcSet={`${alex_carter_420w} 420w, ${alex_carter_500w} 500w`}
            />
            <section className="Team-profile">
                <h2>Alex Carter</h2>
                <p>Lead Developer</p>
                <p>BSc in Ethical Product Development</p>
            </section>
        </section>
    );
}

// Renders Jessica Brotherton's profile
function ProfileJessicaBrotherton() {
    return (
        <section className="Team-container">
            <img 
                style={{maxWidth: "333px"}}
                className="Team-picture" 
                alt="Jessica Brotherton smiling wearingg a black top in front of a white background." 
                src={jessica_brotherton_333w}
            />
            <section className="Team-profile">
                <h2>Jessica Brotherton</h2>
                <p>Head of Finance</p>
                <p>BA in Banking</p>
            </section>
        </section>
    );
}

// Renders Laura Lago's profile
function ProfileLauraLago() {
    return (
        <section className="Team-container">
            <img 
                style={{maxWidth: "333px"}}
                className="Team-picture" 
                alt="Laura Lago smirking wearing a green top and standing in front of a navy blue brickwall." 
                src={laura_lago_333w}
            />
            <section className="Team-profile">
                <h2>Laura Lago</h2>
                <p>Head of Distribution</p>
            </section>
        </section>
    );
}

// Renders team profiles list
function TeamProfilesList() {
    return (
        <section className="Team-list">
            {/* Jennifer Robertson section */}
            <ProfileJenniferRobertson />

            {/* Harry Robertson section */}
            <ProfileHarryRobertson />

            {/* Josh Manningham section */}
            <ProfileJoshManningham />

            {/* Alex Carter section */}
            <ProfileAlexCarter />

            {/* Jessica Brotherton section */}
            <ProfileJessicaBrotherton />

            {/* Laura Lago section */}
            <ProfileLauraLago />
        </section>
    );
}

// Renders Meet the team page
function Team() {
    return (
        <main className="team-page-container">
            <h1>Meet the team</h1>
            <p>Meet the Bio-Store team! A group with very mixed backgrounds coming 
                together with the common cause of wanting the Earth to be a healthier, 
                greener space for all humans, animals and plants to thrive together.</p>
            <TeamProfilesList />
        </main>
    );
}

// Exports Team component
export default Team;
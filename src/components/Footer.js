// Import react
import React from 'react';

// Import images
import greyscale_logo_420w from '../images/logo/GreyscaleLogo-420w.webp';
import greyscale_logo_751w from '../images/logo/GreyscaleLogo-751w.webp';

// Render contact details section
function ContactDetails() {
    return (
        <section>
            <h2>Contact Details</h2>
            <section>
                <h3>Phone</h3>
                <p>0151 883 1122</p>                
            </section>
            <section>
                <h3>Email</h3>
                <p>info@bio-storeexample.co.uk</p>                
            </section>
        </section>
    );
}

// Render opening times section
function OpeningTimes() {
    return(
        <section>
            <h2>Opening Times</h2>
            <p>Monday - Friday (8am - 7pm)</p>
            <p>Saturday (8am - 5pm)</p>
            <p>Sunday (12pm - 5pm)</p>
        </section>
    );
}

// Render website credits section
function WebsiteCredits() {
    return (
        <section className="footer-credits">
            <div className="footer-image-container">
                <img 
                    style={{maxWidth: "751px"}}
                    alt="Bio-store logo"
                    src={greyscale_logo_420w}
                    srcSet={`${greyscale_logo_420w} 420w, ${greyscale_logo_751w} 751w`}
                />
            </div>
            <p>Website designed & developed by Nathan Pope</p>
        </section>
    );
}

// Render website footer
function Footer() {
    return (
        <footer>
            {/* Contact details */}
            <ContactDetails />
            {/* Opening times */}
            <OpeningTimes />
            {/* Website credits */}
            <WebsiteCredits />
        </footer>
    );
}

// Export Footer component
export default Footer;
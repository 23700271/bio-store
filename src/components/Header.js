// Import react
import React from 'react';

// Import React Router Link and NavLink
import {
    Link,
    NavLink
} from 'react-router-dom';

// Import images
import greyscale_logo_420w from '../images/logo/GreyscaleLogo-420w.webp';
import greyscale_logo_751w from '../images/logo/GreyscaleLogo-751w.webp';

// Renders the navbar and handles state change for navbar visibility on mobile.
class Navbar extends React.Component {
    constructor() {
        super();
        this.state = {
        showNavbar: false
        }
    }

    // Alternates visibility of hamburger navigation bar when clicked
    changeVisibility() {
        // Change state to opposite of current value
        this.setState({showNavbar: !this.state.showNavbar})
    }
    
    // Render the navbar
    render() {
        return (
            <nav>
                {/* Contact button */}
                <Link className="link-btn" to="/contact">Contact</Link>

                {/* Hamburger nav icon */}
                <div id="nav-icon" className={ this.state.showNavbar ? "open" : ""} onClick={this.changeVisibility.bind(this)}>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>

                {/* Navigation links */}
                <ul className={ this.state.showNavbar ? "show-nav" : "hide-nav"}>
                    <li onClick={this.changeVisibility.bind(this)}><NavLink  className="App-link" activeClassName="active" exact to="/">Home</NavLink></li>
                    <li onClick={this.changeVisibility.bind(this)}><NavLink className="App-link" activeClassName="active" exact to="/about">About</NavLink></li>
                    <li onClick={this.changeVisibility.bind(this)}><NavLink className="App-link" activeClassName="active" exact to="/team">Team</NavLink></li>
                    <li onClick={this.changeVisibility.bind(this)}><NavLink className="App-link" activeClassName="active" exact to="/products">Products</NavLink></li>
                </ul>
            </nav>
        );
    }
}

// Renders the page header
function Header() {
    return (
        <header className="App-header">
            {/* Logo */}
            <Link to="/">
                <img 
                    style={{maxWidth: "751px"}}
                    alt="Bio-store logo"
                    src={greyscale_logo_420w}
                    srcSet={`${greyscale_logo_420w} 420w, ${greyscale_logo_751w} 751w`}
                />
            </Link>

            {/* Navigation bar */}
            <Navbar />
        </header>
    );
}

// Export Header component
export default Header;